# Version 0.4.0

[Official changelog](https://github.com/Plume-org/Plume/releases/tag/0.4.0-alpha-4), and for our custom image :

* Add a custom Dockerfile based on the official one with a HEALTCHECK, psql client and static env variables
* Add a custom entrypoint to automatically run the migrations at first launch only and run migration when updating
* Clean separation of networks
* Adding a tag to fix the version of image
* Configure non-secret environment with Docker Compose
* Update database to PG v12
