ARG VERSION
FROM plumeorg/plume:${VERSION}

ARG PLUME_VERSION
ENV MIGRATION_DIRECTORY=migrations/postgres
ENV USE_HTTPS=1
ENV ROCKET_ADDRESS=0.0.0.0
ENV ROCKET_PORT=7878
ENV RUST_BACKTRACE=full
ENV FIRSTLAUNCH_PATH=/firstlaunch/yeah

RUN apt-get --allow-releaseinfo-change update && \
    # See https://github.com/dalibo/temboard/issues/211#issuecomment-342205157 to understand why
    mkdir -p /usr/share/man/man1 && \
    mkdir -p /usr/share/man/man7 && \
    apt-get install -y postgresql-client curl && \
    rm -rf /var/lib/apt/lists/*

COPY ./entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD plume
